pcp() {
ssh postgres@10.200.1.60 PCPPASSFILE=/etc/pgpool-II/.pcppass $* --no-password
}

if [ -z "$1" ] ; then
	ssh postgres@10.200.1.60 'ls /usr/bin/pcp_*' | cut -d'/' -f4
else
	pcp $*
fi
