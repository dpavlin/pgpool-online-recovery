#!/bin/sh -xe

cat ~/.ssh/id_rsa.pub > /tmp/authorized_keys

ssh_config() {
echo StrictHostKeyChecking=no | ssh root@$1 'sudo -u postgres cat > /var/lib/pgsql/.ssh/config ; test -f /var/lib/pgsql/.ssh/id_rsa || sudo -u postgres ssh-keygen -f /var/lib/pgsql/.ssh/id_rsa -t rsa -N ""'
ssh root@$1 'cat /var/lib/pgsql/.ssh/id_rsa.pub' >> /tmp/authorized_keys
}

ssh_config 10.200.1.61
ssh_config 10.200.1.62

for host in 10.200.1.61 10.200.1.62
do
	ssh root@$host 'cat > /var/lib/pgsql/.ssh/authorized_keys' < /tmp/authorized_keys
done

