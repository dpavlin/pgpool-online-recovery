create table t (
id serial,
a text not null,
b int,
t timestamp default now()
);

insert into t (a,b) values ('a',1);
insert into t (a,b) values ('a',2);
