#!/bin/sh

id=`psql -h 10.200.1.60 -p 9999 -t -c "select max(id)+1 from t" test postgres`

while true ; do
	psql -h 10.200.1.60 -p 9999 -t \
		-c "insert into t (a,b) values ('t',$id)" \
		-c "select id,a,b,now()-t from t order by id desc limit 1" \
		test postgres
	id=`expr $id + 1`
	sleep 1
done
