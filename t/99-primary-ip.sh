NODE=`psql -h 10.200.1.60 -p 9999 -c 'show pool_nodes' postgres postgres | grep primary | awk '{ print $3 }'`
if [ -z "$NODE" ] ; then
	echo 10.200.1.60
else
	grep $NODE hosts | awk '{ print $1 }'
fi
