#!/bin/sh -xe

export PAGER=''

# test database
psql -h 10.200.1.60 -p 9999 -c 'create database test' --user postgres

psql -h 10.200.1.60 -p 9999 --user postgres -l

psql -h 10.200.1.60 -p 9999 -f t/1-create-test.sql test postgres 

psql -c 'SELECT * from pg_stat_replication' -h 10.200.1.60 -p 9999 postgres postgres

psql -h 10.200.1.60 -p 9999 -c 'select * from t' test postgres
psql -h 10.200.1.61         -c 'select * from t' test postgres
psql -h 10.200.1.62         -c 'select * from t' test postgres

