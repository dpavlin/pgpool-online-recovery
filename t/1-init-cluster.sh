#!/bin/sh -xe

cat << __STEP_1__ > /tmp/1.sh
systemctl stop postgresql-9.6
rm -Rf /var/lib/pgsql/9.6/{data,archive}
mkdir /var/lib/pgsql/9.6/{data,archive}
chown postgres:postgres /var/lib/pgsql/9.6/{data,archive}
su postgres -c "/usr/pgsql-9.6/bin/initdb --lc-collate=hr_HR.utf8 -D /var/lib/pgsql/9.6/data/"

systemctl enable postgresql-9.6
__STEP_1__

cat << __STEP_2__ > /tmp/2.sh
systemctl start postgresql-9.6
psql -c "create user replication with password 'replication123' login replication" postgres postgres
__STEP_2__

scp /tmp/1.sh /tmp/2.sh root@10.200.1.61:/tmp/
scp /tmp/1.sh /tmp/2.sh root@10.200.1.62:/tmp/
ssh root@10.200.1.61 sh -xe /tmp/1.sh

ssh root@10.200.1.62 sh -xe /tmp/1.sh

make push-pg

ssh root@10.200.1.61 ln -sf /var/lib/pgsql/9.6/data/postgresql.conf.master /var/lib/pgsql/9.6/data/postgresql.conf
ssh root@10.200.1.61 sh -xe /tmp/2.sh

ssh root@10.200.1.62 ln -sf /var/lib/pgsql/9.6/data/postgresql.conf.slave /var/lib/pgsql/9.6/data/postgresql.conf
ssh root@10.200.1.62 sudo -u postgres /var/lib/pgsql/streaming-replication.sh 10.200.1.61

