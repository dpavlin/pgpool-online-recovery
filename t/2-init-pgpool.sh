#!/bin/sh -xe

export PAGER=''

install() {
	scp pgpool.service root@$1:/usr/lib/systemd/system/pgpool.service

	cat <<__SHELL__ > /tmp/3.sh
	systemctl stop pgpool

	mkdir -p /var/log/pgpool

	mkdir -pv /etc/pgpool-II/sbin
	cp /usr/sbin/ip /etc/pgpool-II/sbin/
	cp /usr/sbin/arping /etc/pgpool-II/sbin/
	chmod -v 4755 /etc/pgpool-II/sbin/*

	rpm -ql pgpool-II-pg96 && systemctl stop pgpool || \
	yum -y install http://www.pgpool.net/yum/rpms/3.6/redhat/rhel-7-x86_64/pgpool-II-pg96-3.6.6-1pgdg.rhel7.x86_64.rpm http://www.pgpool.net/yum/rpms/3.6/redhat/rhel-7-x86_64/pgpool-II-pg96-extensions-3.6.6-1pgdg.rhel7.x86_64.rpm

	systemctl enable pgpool

	# create pcp.conf
	pg_md5 postgres | xargs -i echo "postgres:{}" > /etc/pgpool-II/pcp.conf
	echo localhost:9898:postgres:postgres > /etc/pgpool-II/.pcppass
	chmod 600 /etc/pgpool-II/.pcppass
	chown postgres /etc/pgpool-II/.pcppass

__SHELL__
	scp /tmp/3.sh root@$1:/tmp/
	ssh root@$1 sh -x /tmp/3.sh

}

install 10.200.1.61
install 10.200.1.62

make push-pgpool

ssh root@10.200.1.61 'systemctl stop pgpool ; rm -v /var/log/pgpool/pgpool_status /tmp/.s.PGSQL.9* ; systemctl start pgpool'
ssh root@10.200.1.62 'systemctl stop pgpool ; rm -v /var/log/pgpool/pgpool_status /tmp/.s.PGSQL.9* ; systemctl start pgpool'

