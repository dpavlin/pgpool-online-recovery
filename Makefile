PRIMARY=$(shell ./t/99-primary-ip.sh)

all:
	@echo "make push"
	PAGER= psql -h 10.200.1.60 -p 9999 -c 'show pool_nodes' postgres postgres
	PAGER= psql -h $(PRIMARY) -c 'SELECT * from pg_stat_replication' postgres postgres

init:
	sh -xe ./t/0-ssh-deploy.sh
	sh -xe ./t/1-init-cluster.sh
	sh -xe ./t/2-init-pgpool.sh

pull:
	exit 1

	scp root@10.200.1.60:/etc/pgpool-II/pgpool.conf .
	scp root@10.200.1.60:/etc/pgpool-II/failover.sh .
	scp root@10.200.1.60:/etc/pgpool-II/online-recovery.sh .

	scp root@10.200.1.61:/var/lib/pgsql/streaming-replication.sh .
	scp root@10.200.1.61:/var/lib/pgsql/9.6/data/postgresql.conf.master .
	scp root@10.200.1.61:/var/lib/pgsql/9.6/data/postgresql.conf.slave .
	scp root@10.200.1.61:/var/lib/pgsql/9.6/data/recovery.conf .

push: push-pg push-pgpool

push-pgpool:
	# replace names in config with real ones
	cat pgpool.conf | sed -e 's/__HOSTNAME__THIS__/edozvola-db-01/' -e 's/__HOSTNAME__OTHER__/edozvola-db-02/' > /tmp/pgpool.conf
	scp /tmp/pgpool.conf failover.sh online-recovery.sh command.sh root@10.200.1.61:/etc/pgpool-II/

	cat pgpool.conf | sed -e 's/__HOSTNAME__THIS__/edozvola-db-02/' -e 's/__HOSTNAME__OTHER__/edozvola-db-01/' > /tmp/pgpool.conf
	scp /tmp/pgpool.conf failover.sh online-recovery.sh command.sh root@10.200.1.62:/etc/pgpool-II/

push-pg:
	scp streaming-replication.sh root@10.200.1.61:/var/lib/pgsql/
	scp postgresql.conf.master postgresql.conf.slave recovery.done pg_hba.conf root@10.200.1.61:/var/lib/pgsql/9.6/data/
	ssh root@10.200.1.61 chown -R postgres:postgres /var/lib/pgsql/9.6/data/
	scp sudoers.d/postgres root@10.200.1.61:/etc/sudoers.d/

	#ssh root@10.200.1.61 ln -sf /var/lib/pgsql/9.6/data/postgresql.conf.master /var/lib/pgsql/9.6/data/postgresql.conf


	scp streaming-replication.sh root@10.200.1.62:/var/lib/pgsql/
	scp postgresql.conf.master postgresql.conf.slave recovery.conf pg_hba.conf root@10.200.1.62:/var/lib/pgsql/9.6/data/
	ssh root@10.200.1.62 chown -R postgres:postgres /var/lib/pgsql/9.6/data/
	scp sudoers.d/postgres root@10.200.1.62:/etc/sudoers.d/

	#ssh root@10.200.1.62 ln -sf /var/lib/pgsql/9.6/data/postgresql.conf.slave /var/lib/pgsql/9.6/data/postgresql.conf

restart:
	#ssh root@10.200.1.61 systemctl restart postgresql-9.6
	#ssh root@10.200.1.62 systemctl restart postgresql-9.6
	ssh root@10.200.1.61 'systemctl stop pgpool ; rm -v /var/log/pgpool/pgpool_status /tmp/.s.PGSQL.9* ; systemctl start pgpool'
	ssh root@10.200.1.62 'systemctl stop pgpool ; rm -v /var/log/pgpool/pgpool_status /tmp/.s.PGSQL.9* ; systemctl start pgpool'

status:
	ssh root@10.200.1.61 systemctl status pgpool
	ssh root@10.200.1.62 systemctl status pgpool
	ssh root@10.200.1.61 systemctl status postgresql-9.6
	ssh root@10.200.1.62 systemctl status postgresql-9.6

fix:
	ssh root@10.200.1.60 sudo -u postgres sh -xe /etc/pgpool-II/online-recovery.sh
