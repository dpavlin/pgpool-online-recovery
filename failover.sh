#!/bin/bash
failed_node=$1
new_master=$2
old_primary=$3
trigger_file=$4
new_master_node=$5

(

# if standby goes down.
if [ $failed_node != $old_primary ]; then
    echo "[INFO] Slave node is down. Failover not triggred !";
    exit 0;
fi

echo "[INFO] Remove old master `sudo -u postgres PCPPASSFILE=/etc/pgpool-II/.pcppass pcp_detach_node --host localhost --port 9898 --username postgres --no-password --node-id=$failed_node`"

# Create the trigger file if primary node goes down.
echo "[INFO] Master node is down. Performing failover..."
ssh -i /var/lib/pgsql/.ssh/id_rsa -o StrictHostKeyChecking=no postgres@$new_master "touch $trigger_file"

echo "[INFO] Add new master `sudo -u postgres PCPPASSFILE=/etc/pgpool-II/.pcppass pcp_promote_node --host localhost --port 9898 --username postgres --no-password --node-id=$new_master_node`"

exit 0;

2>&1) | tee /tmp/failover.log
